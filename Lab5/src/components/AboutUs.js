import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import { width } from '@mui/system';

function AboutUs(props) {


    return(
        <Box sx ={{ width: "60%" }}>
            <Paper elevation={3}>
                <h2> จัดทำโดย : { props.name} </h2>
                <h3> ติดต่อ{props.name}ได้ { props.address} </h3>
                <h4> ปล.{props.province} </h4>
            </Paper>
        </Box>
    );
}

export default AboutUs;