import { useState } from "react";
import LuckyNumber from "../components/LuckyNumber";


function LuckyNumberPage() {

    const [ number, setNumber ] = useState("");
    const [ answer, setAnswer ] = useState("");

    function guess() {
        let n = parseInt(number);
        let answer = n;
        setAnswer(answer);
        if( number == 69 ) {
            setAnswer("ถูกแล้วจ้า")
        }else {
            setAnswer("ผิด!!")
        }
    }

    return (
        <div>
            กรุณาทายตัวเลขที่ต้องการทายระหว่าง 0-99 <br /><br />
            <input type="text"  value={number} 
            onChange={ (e) => { setNumber(e.target.value); } } /> <br /><br />

            <button onClick={ ()=> { guess() } }> ทาย </button>

            { answer !=0 &&
                <div>
                    <hr />
                    <LuckyNumber answer = {answer} />
                </div>

            }
        </div>
    );
}

export default LuckyNumberPage;