
function BMIResult (props) {


    return (

        <div>
            <h3>คุณ : {props.name}</h3>
            <h3>ค่า BMI : {props.bmi}</h3>
            <h3>ผล : {props.result}</h3>
        </div>
    )
}

export default BMIResult;