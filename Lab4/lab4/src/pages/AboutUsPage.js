import AboutUs from "../components/AboutUs";

function AboutUsPage() {

    return(
        <div>
            <div align="center">
                <h2> คณะผู้จัดทำเว็บนี้ </h2>
                <hr />

                <AboutUs name="ญาลินดา"
                  address="ทุกที่"
                  province="อะไรสักอย่างซัมติง"/>
                  <hr />
                  <AboutUs name="ทอยอ"
                  address="ทุกที่"
                  province="อะไรสักอย่างซัมติง"/>

            </div>
        </div>
    );
}

export default AboutUsPage;