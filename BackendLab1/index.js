const express = require('express')
const app = express()
const port = 4000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.post('/bmi', (req, res) => {

    let weight = parseFloat(req.query.weight)
    let height = parseFloat(req.query.height)
    var result = {}

    if ( !isNaN(weight) && !isNaN(height) ) {
       let bmi = weight / (height * height)

        result = {
            "status" : 200,
            "bmi" : bmi
        } 
    }else {

        result = {
            "status" : 400,
            "message"  : "weight or height is not a number"
        }
    }
    
    res.send(JSON.stringify(result))
})

app.get('/triangle', (req, res) => {

    let height = parseFloat(req.query.height)
    let base = parseFloat(req.query.base)
    var result = {}

    if ( !isNaN(height) && !isNaN(base) ) {
       let triangle = 1/2 * height * base

        result = {
            "status" : 200,
            "triangle" : triangle
        } 
    }else {

        result = {
            "status" : 400,
            "message"  : "height or base is not a number"
        }
    }
    
    res.send(JSON.stringify(result))

})

app.post('/score', (req, res) => {

  let score = parseFloat(req.query.score)
  var grade ={}

  if (!isNaN(score)){

    if(score >= 80){
      grade = "A"
    }else if(score >= 70){
      grade = "B"
    }else if(score >= 60){
      grade = "C"
    }else if(score >= 50){
      grade = "D"
    }else if(score < 50 ){
      grade = "E"
    }
  }else {
      grade = "status : error"
    }
  
  res.send(JSON.stringify(req.query.name + " ได้เกรด "+ grade))

})

app.post('/hello', (req, res) => {
    res.send('Hello! ' + req.query.name)
  })

  app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})