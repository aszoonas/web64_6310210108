import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import Body from './components/Body';
import Img from './components/Img';

function App() {
  return (
    <div className="App">
      <Header />
      <Img />
      <Body />
      <Footer />
    </div>
  );
}

export default App;
