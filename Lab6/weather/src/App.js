import logo from './logo.svg';
import './App.css';

import { Box, AppBar, Toolbar, Typography } from '@mui/material';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import axios from 'axios';

import { useEffect, useState } from 'react';
import ReactWeather, { useOpenWeather } from 'react-open-weather';
import YouTube from 'react-youtube';

function App() {

  const [ temperature, setTemperature ] = useState();

  const weatherAPIBaseUrl ="http://api.openweathermap.org/data/2.5/weather?";
  const city = "Songkhla";
  const apiKey = "a6816b38cda3c94bbbe5623b11047c16";
  // Open weatherMap API Key  a6816b38cda3c94bbbe5623b11047c16
  
  const { data, isLoading, errorMessage } = useOpenWeather({
    key: 'a6816b38cda3c94bbbe5623b11047c16',
    lat: '48.137154',
    lon: '11.576124',
    lang: 'en',
    unit: 'metric', // values are (metric, standard, imperial)
  });

  useEffect( () => {
    setTemperature ("-------");

    axios.get(weatherAPIBaseUrl+"q="+city+"&appid="+apiKey).then ( (response)=> {
      let data = response.data;
      let temp = data.main.temp - 273;
      setTemperature(temp.toFixed(2));
    })
  }
  , [] );


  return (
    <Box sx = {{ flexGrow : 1, width : "100%" }}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant = "h6">
              WeatherApp
          </Typography>

        </Toolbar>

      </AppBar>

      <Box sx = {{ justifyContent : 'center', marginTop : "20px", width : "100%" ,display: "flex" }}>
          <Typography variant="h4">
              อากาศหาดใหญ่วันนี้
          </Typography> 
      </Box>  
      
        <Box sx = {{ justifyContent : 'center', marginTop : "20px", width : "100%" ,display: "flex" }}>

          <Card sx={{ Width: 275 }}>
                  <CardContent>
                    <Typography sx={{ fontSize: 14 }} color="text.secondary" >
                        อุณหภูมิหาดใหญ่วันนี้คือ
                    </Typography>
                    <Typography variant="h5" component="div">
                        { temperature }
                    </Typography>
                    <Typography variant="h6" component="div">
                        องศาเซลเซียส
                    </Typography>
                   
                  </CardContent>
          </Card>
          <ReactWeather
      isLoading={isLoading}
      errorMessage={errorMessage}
      data={data}
      lang="en"
      locationLabel="Munich"
      unitsLabels={{ temperature: 'C', windSpeed: 'Km/h' }}
      showForecast
    />
        </Box> 

        <YouTube videoId ="UwxatzcYf9Q" /> 
      

    </Box>
    
  );
}

export default App;
